const express = require('express');
const { request } = require("http");

//creamos la aplicacion express
const app = express();
const path = require('path');

app.use(express.static('public'));


app.set( 'view engine' , 'hbs' );
app.set('views', __dirname+'/views');

//Raiz
app.get("/", (request,response)=>{
    response.render("index");
});

//Marcio
app.get("/paginas/3850665/index.html", (request,response)=>{
    response.render("3850665/index");
});

//Lugan
app.get("/paginas/Y18624/index.html", (request,response)=>{
    response.render("Y18624/index");
});

//Cristhian
app.get("/paginas/Y25366/index.html", (request,response)=>{
    response.render("Y25366/index");
});

//Steven
app.get("/paginas/Y12887/index.html", (request,response)=>{
    response.render("Y12887/index");
});

//Wordcloud
app.get("/paginas/wordcloud/index.html", (request,response)=>{
    response.render("wordcloud/index");
});

//Info Curso
app.get("/paginas/info_curso/index.html", (request,response)=>{
    response.render("info_curso/index");
});


// comentaremos esto como prueba
//app.use(express.static(path.join(__dirname, 'public')));

// iniciar app escuchando puerto parametro
app.listen(3000, () => {
    console.log("Servidor corriendo en el puerto 3000");
});